import { Outlet } from "react-router-dom";
import Navigation from "../components/navigation";

const MainLayouts = ({ click, menu }) => {
  return (
    <>
      <Navigation click={click} menu={menu} />
      <Outlet />
    </>
  );
};

export default MainLayouts;
