export async function req(url) {
  const res = await fetch(url);
  return await res.json();
}

export function ucFirst(str) {
  if (str !== undefined) {
    return str[0].toUpperCase() + str.slice(1);
  } else {
    return;
  }
}

export function wordSlice(str) {
  if (str !== undefined) {
    const slice = str.slice(0, -1);
    return slice;
  } else {
    return;
  }
}

export function searchId(url) {
  if (url.includes("character")) {
    return {
      category: "character",
    };
  } else if (url.includes("location")) {
    return {
      category: "location",
    };
  } else if (url.includes("episode")) {
    return {
      category: "episode",
    };
  }
}

export function createNewArray(oldArr) {
  if (Array.isArray(oldArr)) return null;
  let [...arr] = oldArr.results;
  arr = arr.map((obj) => {
    const { category } = searchId(obj.url);
    return {
      title: obj.name,
      type: obj.type || "",
      dimension: obj.dimension || "",
      residents: obj.residents || "",
      air_date: obj.air_date || "",
      episode: obj.episode || "",
      id: obj.id,
      url: obj.url,
      image: obj.image || "",
      category,
    };
  });
  return arr;
}
