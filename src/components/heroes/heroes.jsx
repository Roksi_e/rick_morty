import { Link, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import HeaderCard from "../headerCard";

import { req } from "../../methods";

import styles from "./heroes.module.css";

const Heroes = ({ url }) => {
  const { id } = useParams();
  const [films, setFilms] = useState({
    title: null,
    url: null,
    id: null,
  });
  const [place, setPlace] = useState({});
  const [details, setDetails] = useState({});

  useEffect(() => {
    req(`${url}${id}`)
      .then((e) => {
        setDetails({
          all_episode: e.episode,
          url: e.url,
          title: e.name,
          image: e.image,
          species: e.species,
          gender: e.gender,
          location: req(`${e.location.url}`).then((info) => {
            setPlace({
              title: info.name,
              url: info.url,
              id: info.id,
              category: "location",
            });
          }),

          episode: req(`${e.episode[0]}`).then((info) => {
            setFilms({
              title: info.name,
              url: info.url,
              id: info.id,
              category: "episode",
            });
          }),
          status: e.status,
        });
      })
      .catch(() => {});
  }, [details, place, id, url]);

  return (
    <main>
      <section>
        <div className={styles.main}>
          <div className={styles.mainWrap}>
            <HeaderCard data={details} />
          </div>

          <div className={styles.infoBox}>
            <img src={details.image} alt={details.title} />
            <div className={styles.details}>
              <div className={styles.detailsBox}>
                <span>Species:</span> {details.species}
              </div>
              <div className={styles.detailsBox}>
                <span>Gender:</span> {details.gender}
              </div>
              <div className={styles.detailsBox}>
                <span>Status:</span> {details.status}
              </div>
              <div className={styles.detailsBox}>
                <span>Location:</span>
                <Link
                  className={styles.link}
                  url={place.url}
                  to={`/${place.category}s/${place.id}`}
                >
                  {place.title}
                </Link>
              </div>
              <div className={styles.detailsBox}>
                <span>First seen in: </span>
                <Link
                  className={styles.link}
                  url={films.url}
                  to={`/${films.category}s/${films.id}`}
                >
                  {films.title}
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default Heroes;
