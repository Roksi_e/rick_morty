import { FiArrowLeft } from "react-icons/fi";
import { Link } from "react-router-dom";

import styles from "./homeBtn.module.css";

const HomeBtn = ({ relative }) => {
  return (
    <Link to=".." relative={relative} className={styles.link}>
      <div className={styles.homeBtn}>
        <FiArrowLeft className={styles.icon} />
        GO BACK
      </div>
    </Link>
  );
};

export default HomeBtn;
