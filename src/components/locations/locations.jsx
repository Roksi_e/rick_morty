import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Cards from "../cards";
import HeaderCard from "../headerCard";

import { req } from "../../methods";

import styles from "./locations.module.css";

const Locations = ({ url }) => {
  const { id } = useParams();
  const [error, setError] = useState("");
  const [isLoading, setisLoading] = useState(true);
  const [person, setPerson] = useState([]);
  const [people, setPeople] = useState([]);
  const [details, setDetails] = useState({});

  useEffect(() => {
    req(`${url}${id}`)
      .then((e) => {
        setDetails({
          id: e.id,
          title: e.name,
          type: e.type || e.episode,
          residents: e.residents || e.characters,
          dimension: e.dimension || e.air_date,
          details: e,
        });
        createPeople(e.residents);
      })
      .catch((error) => setError(error.message))
      .finally(() => setisLoading(false));
  }, [url, id]);

  function createPeople(data) {
    if (!Array.isArray(data)) return null;
    const oneCost = data.map((e) => {
      return req(e)
        .then((info) => {
          setPerson({
            title: info.name,
            image: info.image,
            id: info.id,
            category: "character",
          });
        })
        .catch((error) => setError(error.message))
        .finally(() => setisLoading(false));
    });

    return oneCost;
  }

  useEffect(() => {
    setPeople([...people, person]);
  }, [person]);

  if (error) {
    return <h1 className={styles.error}>Error: {error}</h1>;
  }

  return (
    <main>
      <section>
        <div className={styles.main}>
          <div className={styles.mainWrap}>
            <HeaderCard data={details} />
            <div className={styles.content}>
              <div className={styles.detailsBox}>
                Type:
                <span className={styles.span}>{details.type}</span>
              </div>
              <div className={styles.detailsBox}>
                Dimension:
                <span className={styles.span}>{details.dimension}</span>
              </div>
            </div>
          </div>
          <div className={styles.characrers}>
            <h3 className={styles.charactersTitle}>Residents</h3>
            {isLoading ? (
              <h1 className={styles.error}>Loading...</h1>
            ) : (
              <Cards data={people} />
            )}
          </div>
        </div>
      </section>
    </main>
  );
};

export default Locations;
