import { IoMdClose } from "react-icons/io";

import styles from "./modal.module.css";

const Modal = ({ click, text }) => {
  return (
    <div
      onClick={() => {
        click();
      }}
      className={styles.modal}
    >
      <div className={styles.modalText}>{text}</div>
      <IoMdClose className={styles.modalClose} />
    </div>
  );
};

export default Modal;
