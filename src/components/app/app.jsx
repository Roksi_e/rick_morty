import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import MainLayouts from "../../methods/mainLayouts";
import Main from "../../pages/main";
import NotFound from "../../pages/notFound";
import Characters from "../../pages/characters/characters";
import Footer from "../footer";
import Details from "../../pages/details";

import { req } from "../../methods";

import "./app.css";

const App = () => {
  const [error, setError] = useState("");
  const [isLoading, setisLoading] = useState(true);
  const [title, setTitle] = useState();
  const [menu, setMenu] = useState([{}]);

  useEffect(() => {
    req("https://rickandmortyapi.com/api")
      .then((info) => setMenu(info))
      .catch((error) => setError(error.message))
      .finally(() => setisLoading(false));
  }, []);

  const click = (menu) => {
    setTitle(`${menu}s`);
  };

  if (error) {
    return <h1 className="error">Error: {error}</h1>;
  }

  if (isLoading) {
    return <h1 className="error">Loading...</h1>;
  }

  return (
    <>
      <BrowserRouter>
        <div className="app">
          <Routes>
            <Route path="/" element={<MainLayouts click={click} menu={menu} />}>
              <Route index element={<Main data={menu} />} />
              {Object.entries(menu).map((e, i) => {
                return (
                  <>
                    <Route
                      key={i + "route" + i * 8}
                      path={e[0]}
                      element={<Characters title={title} />}
                    />
                    <Route
                      key={i + "route/id" + i * 8}
                      path={`${e[0]}/:id`}
                      element={<Details url={`${e[1]}/`} />}
                    />
                  </>
                );
              })}
              <Route path="*" element={<NotFound />} />
            </Route>
          </Routes>
          <Footer />
        </div>
      </BrowserRouter>
    </>
  );
};

export default App;
