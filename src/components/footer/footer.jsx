import styles from "./footer.module.css";

const Footer = () => {
  return (
    <footer>
      <div className={styles.footerBox}>
        <div>Make with ❤️ for the MobProgramming team</div>
      </div>
    </footer>
  );
};

export default Footer;
