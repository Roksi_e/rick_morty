import styles from "./button.module.css";

const Button = ({ text, click }) => {
  return (
    <>
      <button
        onClick={() => {
          click();
        }}
        className={styles.button}
      >
        {text.toUpperCase()}
      </button>
    </>
  );
};

export default Button;
