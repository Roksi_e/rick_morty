import HomeBtn from "../homeBtn/homeBtn";

import styles from "./headerCard.module.css";

const HeaderCard = ({ data }) => {
  return (
    <div className={styles.title}>
      <HomeBtn relative="path" />
      <h1>{data.title}</h1>
    </div>
  );
};

export default HeaderCard;
