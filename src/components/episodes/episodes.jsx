import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Cards from "../cards";
import HeaderCard from "../headerCard";

import { req } from "../../methods";

import styles from "./episodes.module.css";

const Episodes = ({ url }) => {
  const { id } = useParams();
  const [error, setError] = useState("");
  const [isLoading, setisLoading] = useState(true);
  const [person, setPerson] = useState([]);
  const [people, setPeople] = useState([]);
  const [details, setDetails] = useState({});

  useEffect(() => {
    req(`${url}${id}`)
      .then((e) => {
        setDetails({
          id: e.id,
          title: e.name,
          episode: e.episode,
          characters: e.characters,
          air_date: e.air_date,
          details: e,
        });
        createPeople(e.characters);
      })
      .catch((error) => setError(error.message))
      .finally(() => setisLoading(false));
  }, [url, id]);

  function createPeople(data) {
    if (!Array.isArray(data)) return null;
    const oneCost = data.map((e) => {
      return req(e)
        .then((info) => {
          setPerson({
            title: info.name,
            image: info.image,
            id: info.id,
            category: "character",
          });
        })
        .catch((error) => setError(error.message))
        .finally(() => setisLoading(false));
    });

    return oneCost;
  }

  useEffect(() => {
    setPeople([...people, person]);
  }, [person]);

  if (error) {
    return <h1 className={styles.error}>Error: {error}</h1>;
  }

  return (
    <main>
      <section>
        <div className={styles.main}>
          <div className={styles.mainWrap}>
            <HeaderCard data={details} />
            <div className={styles.content}>
              <div className={styles.detailsBox}>
                Episode:
                <span className={styles.span}>{details.episode}</span>
              </div>
              <div className={styles.detailsBox}>
                Data:
                <span className={styles.span}>{details.air_date}</span>
              </div>
            </div>
          </div>
          <div className={styles.characrers}>
            <h3 className={styles.charactersTitle}>Cost</h3>
            {isLoading ? (
              <h1 className={styles.error}>Loading...</h1>
            ) : (
              <Cards data={people} />
            )}
          </div>
        </div>
      </section>
    </main>
  );
};

export default Episodes;
