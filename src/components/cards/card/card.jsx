import { Link } from "react-router-dom";

import styles from "./card.module.css";

const Card = ({ info, className }) => {
  if (info.category === "character") {
    return (
      <Link className={styles.link} to={`/${info.category}s/${info.id}`}>
        <div className={className}>
          <img className={styles.img} src={info.image} alt={info.title} />
          <h4 className={styles.title}>{info.title}</h4>
        </div>
      </Link>
    );
  } else if (info.category === "location") {
    return (
      <Link className={styles.link} to={`/${info.category}s/${info.id}`}>
        <div className={className}>
          <h4 className={styles.title}>{info.title}</h4>
          <small>{info.type}</small>
          <h5>Residents: {info.residents.length}</h5>
        </div>
      </Link>
    );
  } else if (info.category === "episode") {
    return (
      <Link className={styles.link} to={`/${info.category}s/${info.id}`}>
        <div className={className}>
          <h4 className={styles.title}>{info.title}</h4>
          <small>{info.air_date}</small>
          <h5>{info.episode}</h5>
        </div>
      </Link>
    );
  }
};

export default Card;
