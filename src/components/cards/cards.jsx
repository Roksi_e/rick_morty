import Card from "./card/card";

import styles from "./cards.module.css";

const Cards = ({ data }) => {
  return (
    <>
      <div className={styles.cards}>
        {Array.isArray(data)
          ? data.map((e, i) => {
              return (
                <Card
                  key={i + "card" + i * 8}
                  info={e}
                  className={styles.cardInfo}
                />
              );
            })
          : null}
      </div>
    </>
  );
};

export default Cards;
