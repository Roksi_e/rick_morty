import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import { useState } from "react";
import NavItem from "./navItem";

import logo from "../../img/logo-black 1.png";

import styles from "./navigation.module.css";

const Navigation = ({ click, menu }) => {
  const [closeHead, setCloseHead] = useState(false);

  const closeMenu = () => {
    setCloseHead(true);
  };

  return (
    <>
      <header>
        <div className={styles.wrap}>
          <div className={styles.header}>
            <div className={styles.headerLogo}>
              <img src={logo} alt="logo" />
            </div>
            <div
              className={styles.mobileBtn}
              onClick={() => {
                setCloseHead(!closeHead);
              }}
            >
              {!closeHead ? (
                <AiOutlineMenu
                  className={styles.closeMenu}
                  onClick={() => {
                    closeMenu();
                  }}
                />
              ) : (
                <AiOutlineClose
                  className={styles.closeMenu}
                  onClick={() => {
                    closeMenu();
                  }}
                />
              )}
            </div>
            <div
              className={styles.headerPop}
              style={!closeHead ? { left: "-100%" } : { left: "0px" }}
            >
              <div
                className={
                  closeHead
                    ? [styles.navBox, styles.activeNav].join(" ")
                    : [styles.navBox]
                }
              >
                <ul className={styles.navigation}>
                  {Object.keys(menu).map((e, i) => {
                    return (
                      <NavItem click={click} key={i + 8 + "menu"} menu={e} />
                    );
                  })}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Navigation;
