import { NavLink } from "react-router-dom";

import { ucFirst, wordSlice } from "../../../methods";

import styles from "./navItem.module.css";

const NavItem = ({ menu, click }) => {
  return (
    <li
      onClick={() => {
        click(wordSlice(menu));
      }}
    >
      <NavLink
        to={menu}
        className={({ isActive }) => (isActive ? styles.active : styles.link)}
      >
        {ucFirst(menu)}
      </NavLink>
    </li>
  );
};

export default NavItem;
