import { useEffect, useState } from "react";
import Button from "../../components/button";
import Cards from "../../components/cards";
import HomeBtn from "../../components/homeBtn";
import Modal from "../../components/modal";

import { createNewArray, ucFirst, req, wordSlice } from "../../methods";

import styles from "./characters.module.css";

const Characters = ({ title = "characters" }) => {
  const [error, setError] = useState("");
  const [isLoading, setisLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [nextPage, setNextPage] = useState("");
  const [newArray, setNewArray] = useState([]);
  const [allPages, setAllPages] = useState(0);
  const [isFinish, setIsFinish] = useState(false);

  useEffect(() => {
    req(`https://rickandmortyapi.com/api/${wordSlice(title)}`)
      .then((info) => {
        setAllPages(info.info.pages);
        setNewArray(createNewArray(info));
        setNextPage(info.info.next);
      })
      .catch((error) => setError(error.message))
      .finally(() => setisLoading(false));
  }, [title]);

  const next = () => {
    if (currentPage < allPages) {
      setIsFinish(false);
      req(`${nextPage}`)
        .then((info) => {
          setNewArray([...newArray, ...createNewArray(info)]);
          setNextPage(info.info.next);
          setCurrentPage((prevState) => prevState + 1);
        })
        .catch((error) => setError(error.message))
        .finally(() => setisLoading(false));
    } else {
      setIsFinish(true);
    }
  };

  const modalClose = () => {
    setIsFinish(false);
  };

  if (error) {
    return <h1 className={styles.error}>Error: {error}</h1>;
  }

  return (
    <main>
      <section>
        <div className={styles.main}>
          <div className={styles.title}>
            <HomeBtn relative="path" />
            <h1>{ucFirst(title)}</h1>
          </div>
          <div className={styles.cardsBlock}>
            {isLoading ? (
              <h1 className={styles.error}>Loading...</h1>
            ) : (
              <Cards data={newArray} />
            )}
          </div>

          {isFinish ? (
            <Modal
              text="Oops(:  There's nothing else here"
              click={modalClose}
            />
          ) : null}

          <Button click={next} text="next" />
        </div>
      </section>
    </main>
  );
};

export default Characters;
