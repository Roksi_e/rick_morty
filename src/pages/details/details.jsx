import Heroes from "../../components/heroes";
import Locations from "../../components/locations";
import Episodes from "../../components/episodes";

const Details = ({ url }) => {
  if (url.includes("character")) {
    return <Heroes url={url} />;
  } else if (url.includes("location")) {
    return <Locations url={url} />;
  } else if (url.includes("episode")) {
    return <Episodes url={url} />;
  }
};

export default Details;
