import { IoIosSearch } from "react-icons/io";
import { useEffect, useState } from "react";
import Button from "../../components/button";
import Cards from "../../components/cards";
import Modal from "../../components/modal";

import photo1 from "../../img/rick-and-morty2 1.png";

import { createNewArray, req } from "../../methods";

import styles from "./main.module.css";

const Main = ({ data }) => {
  const [error, setError] = useState("");
  const [isLoading, setisLoading] = useState(true);
  const [searchText, setSearchText] = useState("");
  const [searchData, setSearchData] = useState(null);
  const [dataChar, setDataChar] = useState();
  const [nextPageChar, setNextPageChar] = useState("");
  const [allPagesChar, setAllPagesChar] = useState(0);
  const [currentPageChar, setCurrentPageChar] = useState(1);
  const [dataEpi, setDataEpi] = useState();
  const [nextPageEpi, setNextPageEpi] = useState("");
  const [allPagesEpi, setAllPagesEpi] = useState(0);
  const [currentPageEpi, setCurrentPageEpi] = useState(1);
  const [isModal, setIsModal] = useState(false);
  const [isFinish, setIsFinish] = useState(false);
  const [isIconClick, setIsIconClick] = useState(false);
  const [isOpen, setIsOpen] = useState(true);

  useEffect(() => {
    Object.values(data).map((e) => {
      if (e.includes("character")) {
        return req(e)
          .then((data) => {
            setDataChar(createNewArray(data));
            setNextPageChar(data.info.next);
            setAllPagesChar(data.info.pages);
          })
          .catch((error) => setError(error.message))
          .finally(() => setisLoading(false));
      } else if (e.includes("episode")) {
        return req(e)
          .then((data) => {
            setDataEpi(createNewArray(data));
            setNextPageEpi(data.info.next);
            setAllPagesEpi(data.info.pages);
          })
          .catch((error) => setError(error.message))
          .finally(() => setisLoading(false));
      } else {
        return null;
      }
    });
  }, [data]);

  const loadMorePage = () => {
    if (currentPageChar < allPagesChar) {
      setIsFinish(false);
      req(`${nextPageChar}`)
        .then((info) => {
          setDataChar([...dataChar, ...createNewArray(info)]);
          setNextPageChar(info.info.next);
          setCurrentPageChar((prevState) => prevState + 1);
        })
        .catch((error) => setError(error.message))
        .finally(() => setisLoading(false));
    } else {
      setIsFinish(true);
    }
    if (currentPageEpi < allPagesEpi) {
      req(`${nextPageEpi}`)
        .then((info) => {
          setDataEpi([...dataEpi, ...createNewArray(info)]);
          setNextPageEpi(info.info.next);
          setCurrentPageEpi((prevState) => prevState + 1);
        })
        .catch((error) => setError(error.message))
        .finally(() => setisLoading(false));
    }
  };

  function filter(value) {
    if (!value) return;
    const newArray = [...dataChar, ...dataEpi];
    const newChar = newArray.filter((element) => {
      if (typeof element.episode === "string") {
        return element.episode.toLowerCase().includes(value.toLowerCase());
      } else {
        return element.title.toLowerCase().includes(value.toLowerCase());
      }
    });
    if (newChar.length === 0) {
      setIsModal(true);
    } else {
      setIsModal(false);
    }
    return setSearchData(newChar);
  }

  function searchValue(el) {
    if (typeof el.episode === "string") {
      return el.episode;
    } else {
      return el.title;
    }
  }

  const search = (value) => {
    setIsIconClick(false);
    setSearchText(value);
    filter(searchText);
  };

  function iconClick() {
    setIsIconClick(true);
    filter(searchText);
    setSearchText("");
  }

  const modalClose = () => {
    setIsModal(false);
    setIsFinish(false);
  };

  const click = (e) => {
    setIsOpen(!isOpen);
    setSearchText(e.target.textContent);
  };

  const inputClick = () => {
    setIsOpen(true);
  };

  if (error) {
    return <h1 className="error">Error: {error}</h1>;
  }

  if (isLoading) {
    return <h1 className="error">Loading...</h1>;
  }

  return (
    <main>
      <section>
        <div className={styles.main}>
          <div className={styles.mainSearch}>
            <img src={photo1} alt="photo1" />
            <div className={styles.mainInput}>
              <IoIosSearch
                className={styles.icon}
                onClick={() => {
                  iconClick();
                }}
              />
              <input
                onChange={(e) => {
                  search(e.target.value);
                }}
                onClick={inputClick}
                value={searchText}
                type="search"
                placeholder="Filter by name or episode (ex. S01 or S01E02)"
              />
              <ul className={styles.searchList}>
                {searchData && isOpen
                  ? searchData.map((e, i) => {
                      return (
                        <li
                          onClick={click}
                          key={i}
                          className={styles.searchItems}
                        >
                          {searchValue(e)}
                        </li>
                      );
                    })
                  : null}
              </ul>
            </div>
          </div>
          {isModal ? (
            <Modal
              text="Didn't find the right one? Load more"
              click={modalClose}
            />
          ) : null}
          {isFinish ? (
            <Modal
              text="Oops(:  There's nothing else here"
              click={modalClose}
            />
          ) : null}
          {!isIconClick ? null : isLoading ? (
            <h1>Loading...</h1>
          ) : (
            <Cards data={searchData} />
          )}

          <Button click={loadMorePage} text="load more" />
        </div>
      </section>
    </main>
  );
};

export default Main;
