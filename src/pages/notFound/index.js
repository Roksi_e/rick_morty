import HomeBtn from "../../components/homeBtn";

import styles from "./notFound.module.css";

const NotFound = () => {
  return (
    <main>
      <section>
        <div className={styles.main}>
          <div className={styles.title}>
            <HomeBtn relative="route" />
            <h1>Not Found</h1>
          </div>
        </div>
      </section>
    </main>
  );
};

export default NotFound;
